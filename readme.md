# Hitachi

## Requirements

- PHP >= 5.3.7
    - check your php version with **php -v** from command line
- MCrypt PHP Extension
    - Hitachi is using [Laravel PHP framework](http://laravel.com) that uses MCrypt PHP extension
- [MySQL](http://www.mysql.com) - main DataBase
- [Composer](https://getcomposer.org/download/) - Package manager for PHP

## Installation

- Clone the repository
- Install dependencies (from console)
    - run ```composer install```
- To set permissions: ```chmod -R 777 app/storage```
- Create database: ```mysqladmin -u root -p password YOUR PASSWORD create hitachi```
- Run migrations: ```php artisan migrate```
- Run seeder: ```php artisan db:seed```

## How to run it

- Set http server to point to public folder
