<?php

return array(
    'headline' => 'Add New Project',
    'title' => 'Title',
    'content' => 'Content',
    'picture' => 'Picture',
    'parent' => 'Choose Parent',
    'create' => 'Create Project',
    'edit' => 'Edit Project',
    'delete' => 'Delete Project',
    'projects' => array(
        'create' => 'You have successfully created a new project!',
        'edit' => 'The project was updated.',
        'delete' => 'You have deleted an project!',
        
    ),
);  
