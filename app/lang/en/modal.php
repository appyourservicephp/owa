<?php

return array(
    'headline' => 'Add New Category',
    'title' => 'Title',
    'content' => 'Content',
    'picture' => 'Picture',
    'parent' => 'Choose Parent',
    'create' => 'Create Category',
    'edit' => 'Edit Category',
    'delete' => 'Delete Category',
    'categories' => array(
        'create' => 'You have successfully created a new category!',
        'delete' => 'You have deleted a category!',
        'update' => 'You have updated the category!',
        
    ),
);  
