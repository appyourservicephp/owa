<?php

class HistoryController extends BaseController {


    public function getMachineHistory($machineId)
    {
        $history = History::where('machine_id', '=', $machineId)->first();
        $machine = App::make('MachinesController')->getMachines($machineId);

        $changed = array('languages' => array(),
                 'pdf' => array(),
                 'main_image' => false,
                 'header_image' => false,
                 'photos' => array(),
                 'videos' => array()
                );

        if ($machine && $history && $machine->toJson() != $history->machine_object) {

            $historyArray = json_decode($history->machine_object, true);
            $machineArray = json_decode($machine->toJson(), true);

            if (empty($machineArray)) {
                return $changed;
            }

            $machinePath = public_path().'/machines/'.$machineId;

            foreach ($historyArray[0]['machine_languages'] as $key => $historyLanguage) {

                $machineLanguage = $machineArray[0]['machine_languages'][$key];

                if ($historyLanguage['model'] != $machineLanguage['model'] ||
                    $historyLanguage['engine_power'] != $machineLanguage['engine_power'] ||
                    $historyLanguage['weight'] != $machineLanguage['weight'] ||
                    $historyLanguage['backhoe_bucket'] != $machineLanguage['backhoe_bucket'] ||
                    $historyLanguage['note'] != $machineLanguage['note'] ||
                    $historyLanguage['introduction_text'] != $machineLanguage['introduction_text'] ||
                    $historyLanguage['weight'] != $machineLanguage['weight']
                ) {
                    $changed['languages'][] = $historyLanguage['language_id'];
                }

                // pdf
                if ($historyLanguage['hash']['pdf'] != $machineLanguage['hash']['pdf']) {
                    $fileName = Language::find($historyLanguage['language_id'])->slug . '.pdf';
                    $changed['pdf'][] = asset('/machines/'.$machineId . '/' . $fileName);
                }

                // videos
                if (count($historyLanguage['hash']['videos']) > count($machineLanguage['hash']['videos'])) {
                    $changedVideos = array_diff_assoc($historyLanguage['hash']['videos'], $machineLanguage['hash']['videos']);
                } else {
                    $changedVideos = array_diff_assoc($machineLanguage['hash']['videos'], $historyLanguage['hash']['videos']);
                }

                foreach ($changedVideos as $key => $videoHash) {
                    $fileName = $key.'.mp4';
                    $changed['videos'][$historyLanguage['language_id']][$key] = asset('/machines/'.$machineId . '/' . $fileName);
                }
            }

            $historyHash = $historyArray[0]['machine_languages'][0]['hash'];
            $machineHash = $machineArray[0]['machine_languages'][0]['hash'];

            if ($historyHash['main_image'] != $machineHash['main_image']) {
                $changed['main_image'] = 'true';
            }

            if ($historyHash['header_image'] != $machineHash['header_image']) {
                $changed['header_image'] = 'true';
            }

            // photos
            if (count($historyHash['photos']) > count($machineHash['photos'])) {
                $changedPhotos = array_diff($historyHash['photos'], $machineHash['photos']);
            } else {
                $changedPhotos = array_diff($machineHash['photos'], $historyHash['photos']);
            }
            foreach ($changedPhotos as $key => $photoHash) {
                $changed['photos'][] = asset('/machines/'.$machineId . '/' . $key . '.jpg');
            }
        }

        return $changed;
    }
}