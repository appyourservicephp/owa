<?php

class ArticlesController extends BaseController {

    public $articlesService;
    public $pushRequestService;
    public function __construct(ArticlesService $articlesService, PushRequestService $pushRequestService) {
        $this->articlesService = $articlesService;
         $this->pushRequestService = $pushRequestService;
    }

    /*
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index() {
        $cat = Category::where('title', 'news')->first();

        $parent = $this->loop($cat->id);

        $breadcrumbsData = array('pages' => array('news' => 'News'));
        $title = "News";
        return View::make('news.index', array('title' => $title, 'parents' => $parent))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    public function loop($id) {
        $arr = array();
        $results = Category::where('parent_id', $id)->get();
        foreach ($results as $result) {
            $arr[] = array(
                'title' => $result->title,
                'id' => $result->id,
                'children' => $this->loop($result->id)
            );
        }
        return $arr;
    }

    public function show($id) {
        $parent = Category::find($id);

        $articles = Article::where('category_id', $parent->id)->get();

        $children = Category::where('parent_id', $id)->get();

        $breadcrumbsData = array('pages' => array('news' => 'News', 'news/' . $parent->id . '/show' => $parent->title));

        return View::make('news.show', array('children' => $children, 'parent' => $parent, 'articles' => $articles))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate($id) {
        $parent = Category::find($id);
        $breadcrumbsData = array('pages' => array('news' => 'News', 'news/' . $parent->id . '/show' => $parent->title, 'news/' . $parent->id . '/new' => 'Add new'));
        return View::make('news.create_edit', compact('parent'))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate() {
        $parent_id = Input::get('id');
        try {
            $article = $this->articlesService->store(Input::all(), get_class());
            
            $this->setMessage(Lang::get('news.news.create'));
            $this->pushRequestService->push($article->id);            
            return Redirect::to('news/' . $parent_id . '/show');
        } catch (ValidatorException $e) {
            $this->setErrorMessage('There are some errors.');

            return Redirect::back()
                            ->withErrors($e->getErrors())
                            ->withInput(Input::except('pictures','header_picture', 'pdf', 'video'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getEdit($id) {
        $article = Article::find($id);
        $parent = Category::where('id', $article->category_id)->first();
//        $parent = $category->masterCategory;
        $breadcrumbsData = array('pages' => array('news' => 'News', 'news/' . $parent->id . '/show' => $parent->title,
                'news/' . $article->id . '/edit' => 'Edit ' . $article->title));

        return View::make('news.create_edit', compact('parent', 'article'))
                        ->nest('breadcrumbs', 'partials.breadcrumbs', $breadcrumbsData);
        ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postEdit($id) {
        
        $parent_id = Input::get('id');        
                
        try {
           
            $article = $this->articlesService->update(Input::all(), $id );
           
            $this->setMessage(Lang::get('news.news.edit'));
            $this->pushRequestService->push($id);            
            return Redirect::to('news/' . $parent_id . '/show');
        } catch (ValidatorException $e) {
            $this->setErrorMessage('There are some errors.');
            
            return Redirect::back()
                            ->withErrors($e->getErrors())
                            ->withInput(Input::except('pictures','header_picture', 'pdf', 'video'));
        }


//        $rules = Category::$rules;
//        $data = Input::all();
//        $validator = Validator::make($data, $rules);
//        if ($validator->fails()) {
//            return Redirect::back()->withErrors($validator)->withInput();
//        }
//        $parent_id = Input::get('id');
//        $article = Article::find($id);
//        $article->title = Input::get('title');
//        $article->content = Input::get('content');
//        $article->user_id = Auth::user()->id;
//        $article->save();
//
//        $data = Input::file('image');
//        if (Input::hasFile('image')) {
//            if (!empty($data)) {
//                $file = new Document();
//                $entityName = get_class();
//                $file->store($data, $entityName, $article->id);
//            }
//        }
//
//        $this->setMessage(Lang::get('news.news.edit'));
//        return Redirect::to('news/' . $parent_id . '/show');
    }

    public function delete($id) {
        
        
        $news = Article::find($id);
        Product::where('category_id', $news->category_id)->delete();
        $this->articlesService->deleteFiles($news->id);
        $news->delete();
        $this->setMessage(Lang::get('news.news.delete'));
        return Redirect::back();
    }
    
    public function deleteFile($id, $i, $type)
    {
        
        $isFileDeleted = $this->articlesService->deleteFile($id, $i, $type);

        return json_encode(array('deleted' => $isFileDeleted));
    }
    
    public function getAllArticles() {
        return Article::all();
    }


}
