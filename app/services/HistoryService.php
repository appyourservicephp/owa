<?php

class HistoryService
{
    public function store($machineId, $machine)
    {
        History::create(
            array(
                'machine_id' => $machineId,
                'machine_object' => $machine
            )
        );
    }

    public function update($machineId, $machine)
    {
        $machineHistory = History::where('machine_id', '=', $machineId)->first();

        if ($machineHistory) {
            $machineHistory->machine_object = $machine;
            $machineHistory->save();
        } else {
            $this->store($machineId, $machine);
        }
    }
}