<?php

class PushRequestService
{
    public function push($machineId, $languageIds = array())
    {
        $tokens = DeviceToken::all()->lists('token');
        $deviceArray = array();
        $app = 'owa';
        
        foreach($tokens as $token) {
            $deviceArray[] = PushNotification::Device($token);
        }
        
        
        
        if (count($deviceArray) != 0) {
            $devices = PushNotification::DeviceCollection($deviceArray);
            $message = $this->makeMessage($machineId, $languageIds);
                     
            
           
            if (App::environment() == 'production') {
                $app = 'owa_prod';
            }

            PushNotification::app($app)
                    ->to($devices)
                    ->send($message);
             
        }
    }

    private function makeMessage($machineId, $languageIds)
    {
        $updated = json_encode(
            array(
                'id' => $machineId,
                'langs' => array(1),
            )
        );

        return PushNotification::Message('OWA update!', array(
            'badge' => 1,
            'custom' => array('updated' => $updated)
        ));
    }
}
