<?php

class CategoriesService {

    public function getAllCategoriesWithSubcategories() {
        return Category::with('masterCategory', 'subCategories')->get();
    }

    public function storeMainImage($id, $image, $data) {

        $this->validate($data);
        
        $imgPth = public_path() . '/appfiles/categories/' . $id . '/';
        $imgName = 'photo_' . $id . '.' . $image->getClientOriginalExtension();
        $image->move($imgPth, $imgName);
        $ext = $image->getClientOriginalExtension();
        
        $mainPicture = Image::make('appfiles/categories/' . $id . '/photo_' . $id . '.' . $ext);
        $mainPicture->fit(409, 1408);
        $mainPicture->save(public_path() . '/appfiles/categories/' . $id . '/main_picture_thumbnail.jpg');
        
        $filePath = $imgPth . $imgName;
        if (file_exists($filePath)) {
            unlink($filePath);
        }
    }

    public function validate($data) {
        Validator::extend('resolution', 'PhotoValidator@resolution');
        $messages = array(
            'resolution' => 'Resolution is not right.',
        );
        $rules = array(
            'image' => 'image|resolution:621, 458|required',
        );
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            $validators = $validator->errors();
            throw new ValidatorImageException($validators->toArray());
            
        }
    }

}
