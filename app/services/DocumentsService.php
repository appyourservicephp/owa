<?php

class DocumentsService {

    public function store($inputs, $entityName) {


        $this->validateInputs(
                $inputs, Product::getPhotoRules(), Product::getVideoRules(), Product::getPdfRules()
        );

        $parent = $inputs['parent'];

        if (empty($parent)) {
            $parent = NULL;
        }

        $category = Category::create(array('title' => $inputs['title'], 'content' => $inputs['content'], 'parent_id' => $parent));

 
        // add documents
        foreach ($inputs as $index => $input) {
            $i = 1;
            $file = new Document();
            if (substr($index, 0) == "pdf") {
                // save pdf file
                foreach ($input as $pdf) {
                    if (isset($pdf) && !empty($pdf)) {
                        $pdf->move('appfiles/pdf/', $category->id . '_pdf_' . $i . '.pdf');
                        $file->name = $pdf->getClientOriginalName();
                        $file->ext = $pdf->getClientOriginalExtension();
                        $file->entity_id = $category->id;
                        $file->entity_name = $entityName;
                    }
                    $i++;
                }
            }
           
            if (substr($index, 0) == "pictures") {
                // save image file
                foreach ($input as $pic) {
                    if (isset($pic) && !empty($pic)) {  
                        
                        $pic->move('appfiles/images/', $category->id . '_photo_' . $i . '.jpg');                        
                        $file->name = $pic->getClientOriginalName();
                        $file->ext = $pic->getClientOriginalExtension();
                        $file->entity_id = $category->id;
                        $file->entity_name = $entityName;
                    }
                    $i++;
                }
            }

            if (substr($index, 0) == "video") {
                // save video file
                $video = $inputs['video'];
                if (isset($video) && !empty($video)) {
                    $video->move('appfiles/videos/', $category->id . '_video' . '.mp4');
                    $file->name = $video->getClientOriginalName();
                    $file->ext = $video->getClientOriginalExtension();
                    $file->entity_id = $category->id;
                    $file->entity_name = $entityName;
                }
            }
        }
        
            
            $file->save();
        




        return $category;
    }

    private function validateInputs($inputs, $photoRules, $videoRules, $pdfRules) {

        $validators = array();

        // validate photos
        Validator::extend('requireOnePhoto', 'PhotoValidator@requireOnePhoto');
        Validator::extend('resolution', 'PhotoValidator@resolution');
        $messages = array(
            'require_one_photo' => 'Please upload at least one photo with right resolution.',
            'resolution' => 'Resolution is not right.'
        );

        Validator::extend('video', 'VideoValidator@video');
        $videoValidatorMessages = array(
            'video' => 'Please upload mp4 video file.',
        );


        $photoValidator = Validator::make(
                        $inputs, $photoRules, $messages
        );

        if ($photoValidator->fails()) {
            $validators['photo'] = $photoValidator;
        }

        Validator::extend('pdf', 'PdfValidator@pdf');
        $messages = array(
            'pdf' => 'Please upload pdf file.',
        );

        foreach ($inputs as $index => $input) {

            // validate pdf
            for ($i = 1; $i < 3; $i++) {

                if (isset($inputs['pdf'][$i]) && isset($inputs['pdf'][$i]) != '') {

                    $pdfInput = array('pdf' => $inputs['pdf'][$i]);

                    $pdfValidator = Validator::make(
                                    $pdfInput, $pdfRules, $videoValidatorMessages
                    );

                    if ($pdfValidator->fails()) {

                        $validators['pdf' . $i] = $pdfValidator;
                    }
                }
            }
            
            // validate video
            if (isset($inputs['video']) && isset($inputs['video']) != '') {
                $videoInput = array('video' => $inputs['video']);
                $videoValidator = Validator::make(
                                $videoInput, $videoRules, $videoValidatorMessages
                );
                if ($videoValidator->fails()) {

                    $validators['video'] = $videoValidator;
                }
            }


            // validate content

            $inputText = array(
                'title' => $inputs['title'],
                'content' => $inputs['content'],
            );



            $rules = array(
                'title' => 'required',
                'content' => 'required',
            );



            $inputTextValidator = Validator::make($inputText, $rules);

            if ($inputTextValidator->fails()) {

                $validators['title'] = $inputTextValidator;
                $validators['content'] = $inputTextValidator;
            }


            //validate inputs
        }


        if (count($validators) > 0) {

            throw new ValidatorException($validators);
        }
    }

    public function update($inputs, $entityName, $id) {
        $photoRules = Product::getPhotoRules();
        $videoRules = Product::getVideoRules();
        $pdfRules = Product::getPdfRules();
        $docRules = Product::getDocRules();


        $machineUpdate = false;


        // if pictures is not provided skip validation
        if (isset($inputs['pictures'])) {
            $countPictures = 0;
            foreach ($inputs['pictures'] as $value) {
                if ($value === null) {
                    $countPictures += 1;
                }
            }

            if ($countPictures == 3) {
                unset($photoRules['pictures']);
            } else {
                $machineUpdate = true;
            }

            foreach ($inputs['pictures'] as $index => $value) {
                if ($value === '' && $countPictures > 1) {
                    unset($photoRules['pictures']);
                    $dirPath = public_path() . '/appfiles/images/' . $id;
                    $picturePath = $dirPath . '/photo_' . $index . '.jpg';
                    if (file_exists($picturePath)) {
                        unlink($picturePath);
                    }
                    $pictureThumbnailPath = $dirPath . '/photo_' . $index . '_thumbnail.jpg';
                    if (file_exists($pictureThumbnailPath)) {
                        unlink($pictureThumbnailPath);
                    }
                    $machineUpdate = true;
                }
            }
        } else {
            unset($photoRules['pictures']);
        }

        $pdfsToSkipValidation = array();
        foreach ($inputs as $index => $input) {

            // pdf
            for ($i = 1; $i < 4; $i++) {
                if (isset($input['pdf'][$i])) {
                    foreach ($input['pdf'] as $pdf) {
                        if ($pdf == null && isset($id)) {

                            $path = public_path() . '/appfiles/categories/' . $id .  '/pdf/pdf_' . $id . '.pdf';

                            if (file_exists($path)) {
                                $pdfsToSkipValidation[] = $id;
                            }
                        }
                    }
                }
            }
        }



        // validate inputs
        $this->validateInputs($inputs, $photoRules, $videoRules, $pdfRules, $docRules);

        $file = Document::find($id);
        $file->entity_id = $inputs['title'];
        $file->entity_name = $entityName;
        $file->save();
    }

    

    public function deleteFiles($id) {
        // remove files
        $dirPath = public_path() . '/appfiles/categories/' . $id;
        
        $pdfPath = $dirPath . '/pdf_' . $id . '.pdf';
        $picturePath = $dirPath . '/photo_' . $id . '.jpg';
        $picturePathJpeg = $dirPath . '/photo_' . $id . '.jpeg';
        $pngPath = $dirPath . '/photo_' . $id . '.png';
        $mainPicture = $dirPath . '/main_picture_thumbnail.jpg';
        
        $filesPath = array();
        array_push($filesPath, $pdfPath, $picturePathJpeg, $picturePath, $pngPath, $mainPicture);
        
        foreach ($filesPath as $filePath) {
            if (file_exists($filePath)) {
                unlink($filePath);
            }
        }
        //remove folder
        if (is_dir($dirPath)) {
            rmdir($dirPath);
        }

        
    }

    

    public function deletePdf($machine, $language, $pdf) {
        $dirPath = public_path() . '/appfiles/pdf/';

        $pdfPath = $dirPath . '/' . $language . '_pdf_' . $pdf . '.pdf';
        if (file_exists($pdfPath)) {
            unlink($pdfPath);
        }

        return file_exists($pdfPath) ? false : true;
    }

}
