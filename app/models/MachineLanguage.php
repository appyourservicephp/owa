<?php

class MachineLanguages extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'machine_languages';
    protected $hidden = array('created_at',
        'updated_at',
        'machine',
        'language',
        'place');
    protected $appends = array('category_id',
        'main_image',
        'header_image',
        'photos',
        'thumbnails',
        'pdf',
        'docx',
        'videos',
        'order',
        'hash');

    /**
     * Mass fillable fields
     *
     * @var array
     */
    protected $fillable = array(
        'machine_id',
        'language_id',
        'name',
        'intro',
        'material',
        'reaction_to_fire',
        'thickness',
        'colour',
        'light_reflexion',
        'sound_reduction',
        'sound_absorption',
        'resistance_to_fire',
        'created_at',
        'updated_at'
    );

    public function machine() {
        return $this->belongsTo('Product');
    }

    public function language() {
        return $this->belongsTo('Language');
    }

    public function getCategoryIdAttribute() {
        return $this->attributes['category_id'] = $this->machine->category_id;
    }

    public function getMainImageAttribute() {
        return $this->getAssetsPath('/main_picture.jpg');
    }

    public function getHeaderImageAttribute() {
        return $this->getAssetsPath('/header_picture.jpg');
    }

    public function getPdfAttribute() {
        $pdf = array();
        for ($i = 1; $i < 4; $i++) {
            $pdfPath = $this->getMachinePath() . '/' . $this->language->slug . '_pdf_' . $i . '.pdf';
            if (file_exists($pdfPath)) {
                $pdf[] = asset('/machines/' . $this->machine_id . '/' . $this->language->slug . '_pdf_' . $i . '.pdf');
            }
        }
        return $this->attributes['pdf'] = $pdf;
    }
    
    public function getDocxAttribute() {
        $docx = array();
        for ($i = 1; $i < 4; $i++) {
            $docxPath = $this->getMachinePath() . '/' . $this->language->slug .  '_doc_' . $i . '.docx';
            if (file_exists($docxPath)) {
                $docx[] = asset('/machines/' . $this->machine_id . '/' . $this->language->slug . '_doc_' . $i . '.docx');
            }
        }
        return $this->attributes['docx'] = $docx;
    }

    public function getPhotosAttribute() {
        $photos = array();

        for ($i = 1; $i < 11; $i++) {
            $picturePath = $this->getMachinePath() . '/photo_' . $i . '.jpg';

            if (file_exists($picturePath)) {
                $photos[] = $this->getAssetsPath('/photo_' . $i . '.jpg');
            }
        }

        return $this->attributes['photos'] = $photos;
    }

    public function getVideosAttribute() {
        $videos = array();

        for ($i = 1; $i < 4; $i++) {
            $videoPath = $this->getMachinePath() . '/' . $this->language->slug . '_video_' . $i . '.mp4';
            if (file_exists($videoPath)) {
                $videos[] = asset('/machines/' . $this->machine_id . '/' . $this->language->slug . '_video_' . $i . '.mp4');
            }
        }

        return $this->attributes['videos'] = $videos;
    }

    public function getMachinePath() {
        return public_path() . '/machines/' . $this->machine_id;
    }

    public function getOrderAttribute() {
        return $this->attributes['order'] = $this->machine->place;
    }

    private function getAssetsPath($file) {
        return asset('/machines/' . $this->machine_id . $file);
    }

    public function getHashAttribute() {
        $hashes = $this->generateHashes();

        return $this->attributes['category_id'] = array(
            'main_image' => $hashes['main_image'],
//            'pdf' => $hashes['pdf'],
            'photos' => $hashes['photos'],
            'thumbnails' => $hashes['thumbnails'],
            'videos' => $hashes['videos']
        );
    }

    private function generateHashes() {
        $hashes = array();
        $machinePath = $this->getMachinePath();

        $mainImage = $machinePath . '/main_picture.jpg';
        if (is_file($mainImage)) {
            $hashes['main_image'] = md5_file($mainImage);
        } else {
            $hashes['main_image'] = '';
        }



        $photoHashes = array();
        for ($i = 1; $i < 11; $i++) {
            $photo = $machinePath . '/photo_' . $i . '.jpg';
            if (is_file($photo)) {
                $photoHashes['photo_' . $i] = md5_file($photo);
            }
        }
        $hashes['photos'] = $photoHashes;

        $thumbnailHashes = array();
        for ($i = 1; $i < 11; $i++) {
            $thumbnail = $machinePath . '/photo_' . $i . '_thumbnail.jpg';
            if (is_file($thumbnail)) {
                $thumbnailHashes['photo_' . $i] = md5_file($thumbnail);
            }
        }
        $hashes['thumbnails'] = (count($thumbnailHashes) > 0) ? $thumbnailHashes : '';

        $pdfHashes = array();
        for ($i = 1; $i < 4; $i++) {
            $pdf = $machinePath . '/' . $this->language->slug . '.pdf';
            if (is_file($pdf)) {
                $pdfHashes[$this->language->slug . '_pdf_' . $i] = md5_file($pdf);
            } 
        }

        $videoHashes = array();
        for ($i = 1; $i < 4; $i++) {
            $video = $machinePath . '/' . $this->language->slug . '_video_' . $i . '.mp4';
            if (is_file($video)) {
                $videoHashes[$this->language->slug . '_video_' . $i] = md5_file($video);
            }
        }
        $hashes['videos'] = $videoHashes;

        return $hashes;
    }

    public function getThumbnailsAttribute() {
        $thumbnails = array();

        for ($i = 1; $i < 11; $i++) {
            $picturePath = $this->getMachinePath() . '/photo_' . $i . '_thumbnail.jpg';

            if (file_exists($picturePath)) {
                $thumbnails['photo_' . $i] = $this->getAssetsPath('/photo_' . $i . '_thumbnail.jpg');
            }
        }

        return $this->attributes['photos'] = (count($thumbnails) > 0) ? $thumbnails : '';
    }

}
