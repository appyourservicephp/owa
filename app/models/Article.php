<?php

class Article extends \Eloquent {

    protected $fillable = [
        'title',
        'content',
    ];
    static public $rules = array(
        'title' => 'required',
        'content' => 'required',
    );
    protected static $photoRules = array(
        'image' => 'requireOnePhoto:458, 621',
        'pictures' => 'type|requireOnePhoto:458, 621',
        'header_picture' => 'required|image|resolution:2048, 544',
    );
    protected static $videoRules = array(
        'video' => 'video',
    );
    protected static $pdfRules = array(
        'pdf' => 'required|pdf'
    );
    protected $table = 'news';
    protected $appends = array(
        'language_id',
        'material',
        'reaction_to_fire',
        'thickness',
        'colour',
        'light_reflexion',
        'sound_reduction',
        'sound_absorption',
        'resistance_to_fire',
        'pdf',
        'photos',
        'thumbnails',
        'videos',
        'main_image',
        'header_image',
        'order',
        'docx',
        'hash',
    );
    public function getMainImageAttribute() {
        return '';
    }
    public function getOrderAttribute() {
        return '';
    }

    public function getDocxAttribute() {
        return array();
    }

    public function getLanguageIdAttribute() {
        return '1';
    }

    public function getMaterialAttribute() {
        return '';
    }

    public function getReactionToFireAttribute() {
        return '';
    }

    public function getThicknessAttribute() {
        return '';
    }

    public function getColourAttribute() {
        return '';
    }

    public function getLightReflexionAttribute() {
        return '';
    }

    public function getSoundReductionAttribute() {
        return '';
    }

    public function getSoundAbsorptionAttribute() {
        return '';
    }

    public function getResistanceToFireAttribute() {
        return '';
    }

    public function getPhotosAttribute() {

        $res = array();
        for ($i = 1; $i < 6; $i++) {
            $imgPath = public_path() . '/appfiles/news/' . $this->id . '/' . $this->id . '_photo_' . $i . '.jpg';

            if (file_exists($imgPath)) {
                $res[] = asset('/appfiles/news/' . $this->id . '/' . $this->id . '_photo_' . $i . '.jpg');
            }
        }

        return $this->attributes['photos'] = $res;
    }

    public function getThumbnailsAttribute() {

        $res = array();
        for ($i = 1; $i < 6; $i++) {

            $thumbnailPath = public_path() . '/appfiles/news/' . $this->id . '/' . $this->id . '_photo_' . $i . '_thumbnail' . '.jpg';

            if (file_exists($thumbnailPath)) {
                $res[$this->id . '_photo_' . $i] = asset('/appfiles/news/' . $this->id . '/' . $this->id . '_photo_' . $i . '_thumbnail' . '.jpg');
            }
        }

        return $this->attributes['thumbnails'] = (count($res)>0) ? $res : '';
    }

    public function getHeaderImageAttribute() {
        $headerImagePaths = public_path() . '/appfiles/news/' . $this->id . '/header_picture.jpg';
        if (file_exists($headerImagePaths)) {
            $headerImagePath = asset('/appfiles/news/' . $this->id . '/header_picture.jpg');
            return $this->attributes['header_image'] = $headerImagePath;
        }
    }

    public function getPdfAttribute() {

        $res = array();
        for ($i = 1; $i < 3; $i++) {
            $pdfPath = public_path() . '/appfiles/news/' . $this->id . '/' . $this->id . '_pdf_' . $i . '.pdf';
            if (file_exists($pdfPath)) {
                $res[] = asset('/appfiles/news/' . $this->id . '/' . $this->id . '_pdf_' . $i . '.pdf');
            }
        }

        return $this->attributes['pdf'] = $res;
    }

    public function getVideosAttribute() {

        $res = array();
        for ($i = 1; $i < 3; $i++) {
            $pdfPath = public_path() . '/appfiles/news/' . $this->id . '/' . $this->id . '_video_' . $i . '.mp4';
            if (file_exists($pdfPath)) {
                $res[] = asset('/appfiles/news/' . $this->id . '/' . $this->id . '_video_' . $i . '.mp4');
            }
        }

        return $this->attributes['videos'] = $res;
    }
    
    public function getMachinePath() {
        return public_path() . '/appfiles/news/' . $this->id;
    }
    
    public function getHashAttribute() {
        $hashes = $this->generateHashes();

        return $this->attributes['category_id'] = array(
            'main_image' => $hashes['main_image'],
//            'pdf' => $hashes['pdf'],
            'photos' => $hashes['photos'],
            'thumbnails' => $hashes['thumbnails'],
            'videos' => $hashes['videos']
        );
    }

    private function generateHashes() {
        $hashes = array();
        $machinePath = $this->getMachinePath();

        $mainImage = $machinePath . '/main_picture.jpg';
        
        if (is_file($mainImage)) {
            $hashes['main_image'] = md5_file($mainImage);
        } else {
            $hashes['main_image'] = '';
        }



        $photoHashes = array();
        for ($i = 1; $i < 6; $i++) {
            $photo = $machinePath . '/'. $this->id .'_photo_' . $i . '.jpg';
            if (is_file($photo)) {
                $photoHashes[$this->id .'_photo_' . $i] = md5_file($photo);
            }
        }
        $hashes['photos'] = $photoHashes;

        $thumbnailHashes = array();
        for ($i = 1; $i < 6; $i++) {
            $thumbnail = $machinePath . '/'. $this->id .'_photo_' . $i . '_thumbnail.jpg';
            if (is_file($thumbnail)) {
                $thumbnailHashes[$this->id .'_photo_' . $i] = md5_file($thumbnail);
            }
        }
        $hashes['thumbnails'] = (count($thumbnailHashes) > 0) ? $thumbnailHashes : '';

        $pdfHashes = array();
        for ($i = 1; $i < 3; $i++) {
            $pdf = $machinePath . '/' . $this->id .'_pdf_' . $i . '.pdf';
            if (is_file($pdf)) {
                $pdfHashes[$this->id .'_pdf_' . $i] = md5_file($pdf);
            } 
        }

        $videoHashes = array();
        for ($i = 1; $i < 3; $i++) {
            $video = $machinePath . '/' . $this->id . '_video_' . $i . '.mp4';
            if (is_file($video)) {
                $videoHashes[$this->id . '_video_' . $i] = md5_file($video);
            }
        }
        $hashes['videos'] = $videoHashes;

        return $hashes;
    }
    
    
    

    public function categories() {
        return $this->belongsTo('Category', 'category_id');
    }

    public function users() {
        return $this->belongsTo('User', 'user_id');
    }

    public static function getPhotoRules() {
        return self::$photoRules;
    }

    public static function getVideoRules() {
        return self::$videoRules;
    }

    public static function getPdfRules() {
        return self::$pdfRules;
    }

}
