<?php

class Language extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'languages';

    protected $hidden = array('created_at', 'updated_at');

    public function machineLanguages()
    {
        return $this->hasMany('MachineLanguages' , 'language_id');
    }

    public static function getRequiredLanguagesIds()
    {
        return Language::where('required', '=', true)->lists('id');
    }
}