@extends('layouts/main')
 <!--Web site Title--> 
@section('title') {{{ $parent->title }}} :: @parent @stop
@section('content')

{{$breadcrumbs}}

<h1>Project: {{$parent->title}}</h1>
@if(Auth::user()->hasRole('super_admin'))
<div class="pull-right">
    <div class="pull-right">
        <a class="btn btn-sm btn-danger deleteAll" href="{{'/categories/'. $parent->id . '/delete'}}">
            <span class="glyphicon glyphicon-remove"></span>
            Delete {{ $parent->title }} project
        </a>
    </div>
</div>
@endif
<p>
    <a href="/projects/{{$parent->id}}/new" class="btn btn-success">Add new project</a>
</p>

<table class="table table-bordered">
    <thead>
        
        <tr>
            <th>Model</th>
            <th>Title</th>
            <th>Created at</th>
            <th>Controls</th>
        </tr>
        
    </thead>
    @foreach ($projects as $project)
    <tbody id="sortable">
        
         
         <tr class="machine-table-row" data-id="{{$project->id}}">
            
           
                <td class="handle"><span class="move-icon glyphicon glyphicon-resize-vertical"></span></td>
                <td>{{$project->title}}</td>
                <td>{{$project->created_at}}</td>
            
            
            <td width="15%">
                <a href="/projects/{{$project->id}}/edit" class="btn btn-primary">Adjust</a>
                <a href="/projects/{{$project->id}}/delete" class="btn btn-danger pull-right" onclick="return confirm('Are you sure you want to delete this project?')">Remove</a>
            </td>
        </tr>
        
        
    </tbody>
    @endforeach
</table>

@stop