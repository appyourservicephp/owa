@extends('layouts/main')
<!--Web site Title--> 
@section('title') {{{ $category->title }}} :: @parent @stop
@section('content')

{{$breadcrumbs}}

<h1>{{$category->title}} product management</h1>

<?php $about = Product::where('category_id', '4')->first(); ?>
@if(Request::is('about-us') && count($about) > 0)
<p style="margin-bottom: 30px"></p>
@else
@if(Auth::user()->hasRole('super_admin') && $category_id !== '4')
<div class="pull-right">
    <div class="pull-right">
        <a class="btn btn-sm btn-danger deleteAll" href="{{'/categories/'. $category_id . '/delete'}}">
            <span class="glyphicon glyphicon-remove"></span>
            Delete {{ $category->title }} category
        </a>
    </div>
</div>
@endif
<p>
    <a href="/products/{{$category_id}}/new" class="btn btn-success">Add product</a>
</p>
@endif
<table class="table table-bordered">
    <thead>
        @if($category->slug == 'lce')
        <tr>
            <th>&nbsp;</th>
            <th width="75%"></th>
            <th width="15%">Controls</th>
        </tr>
        @else
        <tr>
            <th>Model</th>
            <th>Title</th>
            <th>Created at</th>
            <th>Controls</th>
        </tr>
        @endif
    </thead>
    <tbody id="sortable">
        @foreach ($machines as $machine)

        <tr class="machine-table-row" data-id="{{$machine->id}}">
            <?php $spec = $machine->machineLanguages->first(); ?>
            <?php if ($spec): ?>
                <td class="handle"><span class="move-icon glyphicon glyphicon-resize-vertical"></span></td>
                <td>{{$spec->name}}</td>
                <td>{{$spec->created_at}}</td>
            <?php endif; ?>

            <td width="15%">
                @if(Request::is('about-us') && count($about) > 0)
                <a href="/products/{{$category_id}}/edit/{{$machine->id}}" class="btn btn-primary">Adjust</a>
                @else
                <a href="/products/{{$category_id}}/edit/{{$machine->id}}" class="btn btn-primary">Adjust</a>
                <a href="/delete/{{$machine->id}}" class="btn btn-danger pull-right" onclick="return confirm('Are you sure you want to delete this product?')">Remove</a>
                @endif

            </td>
        </tr>

        @endforeach
    </tbody>
</table>
@stop