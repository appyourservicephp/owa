@extends('layouts/main')
<!--Web site Title--> 
@section('title') {{$title}} :: @parent @stop
@section('content')

{{$breadcrumbs}}
@include('notifications')



<h1>{{$title}}</h1>
<hr>

@foreach($parents as $key => $parent)

@if(count($parent)>0)

<!--<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{$parent['title']}}</h3>
    </div>
    <div class="panel-body">
        <ul class="list-group">
            @foreach($parent['children'] as $children)
<?php $articles = Article::where('category_id', $children['id'])->get(); ?>
            <li class="list-group-item"><a href="{{'/news/'. $children['id']. '/show'}}">{{$children['title']}} <span class="badge pull-right">{{ count($articles)}}</span></a>
            @endforeach
        </ul>
    </div>
</div>-->
<ul class="list-group">

    <?php $news = Article::where('category_id', $parent['id'])->get(); ?>
    <li class="list-group-item"><a href="{{'/news/'. $parent['id']. '/show'}}">{{$parent['title']}} <span class="badge pull-right">{{ count($news)}}</span></a>

</ul>

@else

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{$parent['title']}}</h3>
    </div>
    <div class="panel-body">
        <small>There are no news for this category</small>
    </div>
</div>
@endif
@endforeach

@stop