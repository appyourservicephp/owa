<div class="input-field-group">
    <div class="col-lg-3">
        <label class="control-label pull-right" for="lang_{{$languageId}}[name]">Product name</label>
    </div>
    <div class="col-lg-6">
        <input name="lang_{{$languageId}}[name]" value="{{$input['name'] or ''}}" type="text" class="form-control input-md" id="lang_{{$languageId}}_name" >
    </div>
    <div class="error_lang_{{$languageId}}_name error col-lg-3"></div>
</div>
<div class="input-field-group">
    <div class="col-lg-3">
        <label class="control-label pull-right" for="lang_{{$languageId}}[material]">Material</label>
    </div>
    <div class="col-lg-6">
        <input name="lang_{{$languageId}}[material]" value="{{$input['material'] or ''}}" type="text" class="form-control input-md" id="lang_{{$languageId}}_material" >
    </div>
    <div class="error_lang_{{$languageId}}_material error col-lg-3"></div>
</div>

<div class="input-field-group">
    <div class="col-lg-3">
        <label class="control-label pull-right" for="lang_{{$languageId}}[reaction_to_fire]">Reaction to fire</label>
    </div>
    <div class="col-lg-6">
        <input name="lang_{{$languageId}}[reaction_to_fire]" value="{{$input['reaction_to_fire'] or ''}}" type="text" class="form-control input-md"  id="lang_{{$languageId}}_reaction_to_fire">
    </div>
    <div class="error_lang_{{$languageId}}_reaction_to_fire error col-lg-3"></div>
</div>

<div class="input-field-group">
    <div class="col-lg-3">
        <label class="control-label pull-right" for="lang_{{$languageId}}[thickness]">Thickness</label>
    </div>
    <div class="col-lg-6">
        <input name="lang_{{$languageId}}[thickness]" value="{{$input['thickness'] or ''}}" type="text" class="form-control input-md" id="lang_{{$languageId}}_thickness">
    </div>
    <div class="error_lang_{{$languageId}}_thickness error col-lg-3"></div>
</div>

<div class="input-field-group">
    <div class="col-lg-3">
        <label class="control-label pull-right" for="lang_{{$languageId}}[colour]">Colour</label>
    </div>
    <div class="col-lg-6">
        <input name="lang_{{$languageId}}[colour]" value="{{$input['colour'] or ''}}" type="text" class="form-control input-md" id="lang_{{$languageId}}_colour">
    </div>
    <div class="error_lang_{{$languageId}}_colour error col-lg-3"></div>
</div>

<div class="input-field-group">
    <div class="col-lg-3">
        <label class="control-label pull-right" for="lang_{{$languageId}}[light_reflexion]">Light reflexion</label>
    </div>
    <div class="col-lg-6">
        <input name="lang_{{$languageId}}[light_reflexion]" value="{{$input['light_reflexion'] or ''}}" type="text" class="form-control input-md" id="lang_{{$languageId}}_light_reflexion">
    </div>
    <div class="error_lang_{{$languageId}}_light_reflexion error col-lg-3"></div>
</div>
<div class="input-field-group">
    <div class="col-lg-3">
        <label class="control-label pull-right" for="lang_{{$languageId}}[sound_reduction]">Sound reduction</label>
    </div>
    <div class="col-lg-6">
        <input name="lang_{{$languageId}}[sound_reduction]" value="{{$input['sound_reduction'] or ''}}" type="text" class="form-control input-md" id="lang_{{$languageId}}_sound_reduction">
    </div>
    <div class="error_lang_{{$languageId}}_sound_reduction error col-lg-3"></div>
</div>
<div class="input-field-group">
    <div class="col-lg-3">
        <label class="control-label pull-right" for="lang_{{$languageId}}[sound_absorption]">Sound absorption</label>
    </div>
    <div class="col-lg-6">
        <input name="lang_{{$languageId}}[sound_absorption]" value="{{$input['sound_absorption'] or ''}}" type="text" class="form-control input-md" id="lang_{{$languageId}}_sound_absorption">
    </div>
    <div class="error_lang_{{$languageId}}_sound_absorption error col-lg-3"></div>
</div>
<div class="input-field-group">
    <div class="col-lg-3">
        <label class="control-label pull-right" for="lang_{{$languageId}}[resistance]">Resistance to fire</label>
    </div>
    <div class="col-lg-6">
        <input name="lang_{{$languageId}}[resistance_to_fire]" value="{{$input['resistance_to_fire'] or ''}}" type="text" class="form-control input-md" id="lang_{{$languageId}}_resistance_to_fire">
    </div>
    <div class="error_lang_{{$languageId}}_resistance_to_fire error col-lg-3"></div>
</div>

