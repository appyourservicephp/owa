<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

        <title>@section('title') OWA @show</title>

        <!-- Bootstrap core CSS -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="/css/starter-template.css" rel="stylesheet">
        <link href="/css/style.css" rel="stylesheet">
        <link href="/css/jasny-bootstrap.min.css" rel="stylesheet">
        <link href="/css/jquery.Jcrop.css" rel="stylesheet">
        

        <link href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css" rel="stylesheet">


        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/categories">Owa</a>
                </div>
                <div class="collapse navbar-collapse">
                    @if (Auth::check())
                    <ul class="nav navbar-nav">
                        <li{{ (Request::is('categories*') ? ' class=active' : '') }}><a href="/categories"><span class="glyphicon glyphicon-th-list"></span> Categories</a></li>
                        <li{{ (Request::is('projects*') ? ' class=active' : '') }}><a href="/projects"><span class="glyphicon glyphicon-folder-open"></span> Projects</a></li>
                        <li{{ (Request::is('news*') ? ' class=active' : '') }}><a href="/news"><span class="glyphicon glyphicon-bullhorn"></span> News</a></li>
                        <li{{ (Request::is('products/*') ? ' class=active' : '') }}><a href="/products"><span class="glyphicon glyphicon-cog"></span> Products</a></li>

                        <li{{ (Request::is('logo') ? ' class=active' : '') }}><a href="/logo"><span class="glyphicon glyphicon-leaf"></span> Logo</a></li>
                        @if(Auth::user()->hasRole('super_admin'))
                        <li{{ (Request::is('users*') ? ' class=active' : '') }}><a href="/users"><span class="glyphicon glyphicon-user"></span> Users</a></li>
                        @endif
                        <li{{ (Request::is('about-us*') ? ' class=active' : '') }}><a href="/about-us"><span class="glyphicon glyphicon-info-sign"></span> About Us</a></li>
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <li><a href="/user/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                    </ul>
                    @endif
                </div><!--/.nav-collapse -->
            </div>
        </div>

        @if (Session::has('message'))
        <div class="alert {{ Session::get('message_type') }}">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <center>{{ Session::get('message') }}</center>
        </div>
        @endif

        <div class="container">
            <div class="row">
                @yield('content')
            </div>
        </div><!-- /.container -->

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="/js/jquery-1.11.0.js"></script>
        <script src="/js/jquery.Jcrop.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/jasny-bootstrap.min.js"></script>
        <script src="/js/jquery-ui.js"></script>
        <script src="/js/deleteConf.js"></script>
        <script src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript">
        $(document).ready(function () {
oTable = $('#users').DataTable({
"processing": true,
        "serverSide": true,
        "ajax": "/user/data",
        "columns": [
        {data: 'username', name: 'username'},
        {data: 'email', name: 'email'},
        {data: 'confirmed', name: 'confirmed'},
        {data: 'actions', name: 'actions'}
        ]
});
        oTable = $('#logos').DataTable({
"processing": true,
        "serverSide": true,
        "ajax": "/logos/data",
        "columns": [
        {data: 'title', name: 'title'},
        {data: 'shortcode', name: 'shortcode'},
        {data: 'name', name: 'name'},
        {data: 'actions', name: 'actions'}

        ]
});
        });        </script>
        <script>
                    var myArray = <?php echo isset($results) ? json_encode($results) : 0; ?>;        </script>
        <script src="/js/app.js"></script>
        

    </body>
</html>
