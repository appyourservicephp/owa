<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */


//Route::group(array('before' => 'guest'), function()
//{
//    Route::get('/', 'AuthController@index');
//    Route::get('/login', 'AuthController@index');
//    Route::post('login', 'AuthController@login');
//});

Route::group(array('before' => 'auth'), function() {
    Route::get('/', 'CategoriesController@index');
    Route::get('logout', 'AuthController@logout');
    Route::get('categories', 'CategoriesController@index');
    
    Route::get('categories/create', 'CategoriesController@getCreate');
    Route::post('categories/create', 'CategoriesController@postCreate');
    Route::get('categories/{category_id}/edit', 'CategoriesController@getEdit');
    Route::post('categories/{category_id}/edit', 'CategoriesController@postEdit');
    Route::get('categories/{category_id}/delete', 'CategoriesController@destroy');
    Route::get('delete-category-file/{id}/{type}', 'CategoriesController@deletePdf');
    Route::post('categories/image', 'CategoriesController@postImage');
    
    // ABOUT SU
    Route::get('about-us', 'CategoriesController@getAbout');
    Route::post('about-us/image', 'CategoriesController@postAbout');
    Route::get('about-us/{category_id}/new', 'ProductsController@showAddAbout'); 
    Route::get('about-us/{category_id}/edit/{machine_id}', 'ProductsController@showEditAbout');
    
    
    // PRODUCTS     
    Route::get('products/', 'ProductsController@show');
    Route::get('products/{id}/show', 'ProductsController@index');
    Route::get('products/{category_id}/new', 'ProductsController@showAdd');    
    Route::get('categories-lceformfileds', 'ProductsController@lceformfileds');
    Route::post('add-product', 'ProductsController@doAdd');
    Route::get('products/{category_id}/edit/{machine_id}', 'ProductsController@showEdit');
    Route::post('edit-product', 'ProductsController@doEdit');
    Route::post('update-place', 'ProductsController@updatePlace');
    Route::get('delete/{machine_id}', 'ProductsController@delete');
    Route::get('delete-pdf/{machine}/{language}', 'ProductsController@deletePdf');
    Route::get('delete-video/{machine}/{language}/{video}', 'ProductsController@deleteVideo');
    Route::get('create-thumbnail', 'ImagesController@createThumbnail');
    Route::get('remove-image', 'ImagesController@removeImage');
    Route::get('delete-file/{machine}/{language}/{i}/{type}', 'ProductsController@deleteFile');
    
    // NEWS 
    
    Route::get('news', 'ArticlesController@index');
    Route::get('news/{id}/show', 'ArticlesController@show');
    Route::get('news/{id}/edit', 'ArticlesController@getEdit');
    Route::post('news/{id}/edit', 'ArticlesController@postEdit');
    Route::get('news/{id}/new', 'ArticlesController@getCreate');
    Route::get('news/{id}/delete', 'ArticlesController@delete');
    Route::post('news/create', 'ArticlesController@postCreate');
    Route::get('delete-news-file/{id}/{i}/{type}', 'ArticlesController@deleteFile');    
    Route::get('remove-image-news', 'ImagesController@removeImageNews');
    
    
    // PROJECTS
    
    Route::get('projects', 'ProjectsController@index');
    Route::get('projects/{id}/show', 'ProjectsController@show');
    Route::get('projects/{id}/edit', 'ProjectsController@getEdit');
    Route::post('projects/{id}/edit', 'ProjectsController@postEdit');
    Route::get('projects/{id}/new', 'ProjectsController@getCreate');
    Route::get('projects/{id}/delete', 'ProjectsController@delete');
    Route::post('projects/create', 'ProjectsController@postCreate');
    Route::get('delete-projects-file/{id}/{i}/{type}', 'ProjectsController@deleteFile');
    Route::get('remove-image-projects', 'ImagesController@removeImageProject');
    
    
    // LOGO 
    
     Route::get('logo', 'LogoController@index');
     Route::post('logo', 'LogoController@create');
     Route::get('logos/data', 'LogoController@data');
     Route::get('logo/{id}/delete', 'LogoController@destroy');
    
    
    
});

Route::group(array('prefix' => 'api/v1'), function() {
    Route::get('languages', 'LanguageController@getAllLanguages');
    Route::get('languages/{language_id}', 'LanguageController@getLanguages');
    Route::get('categories', 'CategoriesController@getAllCategories');
    Route::get('logo', 'LogoController@getAllLogos');

    Route::get('products/{id}', 'ProductsController@getProducts');
    Route::get('products/', 'ProductsController@getAllProducts');
    
    Route::get('logo-shortcode', 'LogoController@getShortcode');
    Route::get('news', 'ArticlesController@getAllArticles');
    Route::get('projects', 'ProjectsController@getAllProjects');
    
//    Route::get('machine-history/{machine_id}', 'HistoryController@getMachineHistory');
    Route::resource('device-token', 'DeviceTokensController');
});

// Confide routes
Route::get('users', 'UserController@show');
Route::get('user/create', 'UserController@getCreate');
Route::post('user/create', 'UserController@postCreate');
Route::get('user/{id}/edit', 'UserController@getEdit');
Route::post('user/{id}/edit', 'UserController@postEdit');
Route::get('user/login', 'UserController@login');
Route::post('user/login', 'UserController@do_login');
Route::get('user/confirm/{code}', 'UserController@confirm');
Route::get('user/{id}/delete', 'UserController@delete');
Route::get('user/forgot_password', 'UserController@forgot_password');
Route::post('user/forgot_password', 'UserController@do_forgot_password');
Route::get('user/reset_password/{token}', 'UserController@reset_password');
Route::post('user/reset_password', 'UserController@do_reset_password');
Route::get('user/logout', 'UserController@logout');
Route::get('user/data', 'UserController@data');

App::missing(function()
{
    return Response::view('errors.missing', array(), 404);
});


