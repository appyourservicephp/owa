<?php

if (!function_exists('base64_image_encode')) {
  function base64_image_encode($path)
  {
    if (file_exists($path)) {
        return base64_encode(file_get_contents($path));
    }
  }
}