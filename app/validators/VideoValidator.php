<?php

class VideoValidator
{
    /**
     * if file is provided it must be video
     */
    public function video($field, $value, $params)
    {
        if (isset($value) && gettype($value) == 'object') {
            if ($value->getMimeType() == 'video/mp4' ) {
                return true;
            }
        }
        
        return false;
    }
}
