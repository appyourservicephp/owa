<?php

class PhotoValidator {

    public function requireOnePhoto($field, $value, $params) {
        foreach ($value as $key => $val) {
            if ($val != null) {
                if ($this->resolution($field, $val, $params)) {
                    return true;
                }
            }
        }
        return false;
    }

    public function resolution($field, $value, $params) {
        if ($value->isValid()) {           
            $size = getimagesize($value->getRealPath());

            if ($size[0] >= $params[0] && $size[1] >= $params[1]) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function type($field, $values) {

        $values = array_filter($values);

        if ($values) {
            foreach ($values as $value) {
                $file = $value->getClientOriginalExtension();
                $ext = array('jpg', 'png', 'jpeg');
                if (!in_array($file, $ext)) {
                    return false;
                }
            }
            return true;
        }
    }

}
