<?php

/**
 * Custom validator exception for handling validator failures.
 */
class ValidatorException extends Exception
{
    private $errors;

    public function __construct($validators)
    {
       
        foreach ($validators as $key => $validator) {
            
            $this->errors[$key] = $validator->messages()->toArray();
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }

}
