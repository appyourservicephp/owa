<?php

/**
 * Custom validator exception for handling validator failures.
 */
class ValidatorImageException extends Exception
{
    private $errors;

    public function __construct($validators)
    {
        
        foreach ($validators as $key => $validator) {
           
            $this->errors = $validator;
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }

}
